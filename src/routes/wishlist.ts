import { Router } from 'express';

export const wishlistRoutes = Router()
    .get('/', (req, res) => {
        const wishListElements = [
            {
                productId: 1,
            },
            {
                productId: 2,
            },
            {
                productId: 3,
            }
        ];

        res.json(wishListElements);
    })