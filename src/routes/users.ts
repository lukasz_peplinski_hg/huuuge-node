import { Router } from "express";

const mockUsers = [
  { id: 1, name: "User 1" },
  { id: 2, name: "User 2" },
];

export const usersRoutes = Router()
  .get("/", (req, res) => {
    res.json(mockUsers);
  })

  .get("/:user_id", (req, res, next) => {
    const { user_id } = req.params;
    res.json(mockUsers.find((u) => u.id == parseInt(user_id, 10)));
  })

  .post("/", (req, res, next) => {
    debugger;
    const body = req.body;
    mockUsers.push(body);
    res.send(mockUsers);
  })

  .put("/:user_id", (req, res, next) => {});
