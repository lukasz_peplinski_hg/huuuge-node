import { Router } from "express";


export const promotionsRoutes = Router()

  .get('/', (req, res) => {
    res.send([
      { id: 1, name: 'Promo 1' },
      { id: 2, name: 'Promo 2' },
    ])
  })