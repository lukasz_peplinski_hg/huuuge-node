import express, { RequestHandler, Request, Response, NextFunction } from 'express'
import routes from './routes';

const app = express();

// express.urlencoded()
// express.static

app.use(express.json({
  // limit:'100kb',
}))

app.use(routes);

app.listen(8080);
